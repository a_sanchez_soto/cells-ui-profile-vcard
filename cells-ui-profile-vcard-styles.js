import { css, } from 'lit-element';


export default css`:host {
  display: block;
  box-sizing: border-box;
  @apply --cells-ui-profile-vcard; }

:host([hidden]), [hidden] {
  display: none !important; }

*, *:before, *:after {
  box-sizing: inherit; }

.content {
  position: absolute;
  left: 50%;
  top: 50%;
  -webkit-transform: translate(-50%, -50%);
  transform: translate(-50%, -50%);
  background-color: #1464A5;
  padding: 4%;
  border-radius: 5px;
  text-align: center;
  color: #fff;
  box-shadow: 10px 5px 5px #BDBDBD; }

.titulo {
  font-size: 2rem; }

.sub-titulo {
  font-size: 0.8rem; }

.img-circle {
  border-radius: 50%; }

.img-thumbnail {
  display: inline-block;
  max-width: 60%;
  height: auto;
  padding: 4px;
  line-height: 1.42857143;
  background-color: #fff;
  border: 1px solid #ddd;
  -webkit-transition: all .2s ease-in-out;
  -o-transition: all .2s ease-in-out;
  transition: all .2s ease-in-out; }

.profile-container {
  position: relative;
  width: 100%;
  height: 460px; }
`;