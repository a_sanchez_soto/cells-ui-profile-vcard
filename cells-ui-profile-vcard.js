import { LitElement, html, } from 'lit-element';
import { getComponentSharedStyles, } from '@cells-components/cells-lit-helpers/cells-lit-helpers.js';
import styles from './cells-ui-profile-vcard-styles.js';

import '@bbva-web-components/bbva-button-default';
import '@vcard-components/cells-util-behavior-vcard';

const cellsUtilBehaviorVcard = CellsBehaviors.cellsUtilBehaviorVcard;

/**
This component ...

Example:

```html
<cells-ui-profile-vcard></cells-ui-profile-vcard>
```

##styling-doc

* @customElement
* @polymer
* @LitElement
* @demo demo/index.html
*/
export class CellsUiProfileVcard extends cellsUtilBehaviorVcard(LitElement) {
  static get is() {
    return 'cells-ui-profile-vcard';
  }

  // Declare properties
  static get properties() {
    return {
      user: { type: Object },
    };
  }

  // Initialize properties
  constructor() {
    super();
    this.user = {};
  }

  static get shadyStyles() {
    return `
      ${styles.cssText}
      ${getComponentSharedStyles('cells-ui-profile-vcard-shared-styles').cssText}
    `;
  }

  regresarHome(event){
    console.log('logoSelected', event);
    this.dispatch(this.events.logoSelected, {});
  }

  // Define a template
  render() {
    return html`
      <style>${this.constructor.shadyStyles}</style>
      <slot></slot>
      <div class='profile-container'>
        <div class='content'>
          <div>
            <img src="http://ssl.gstatic.com/accounts/ui/avatar_2x.png" class="avatar img-circle img-thumbnail" alt="avatar">
          </div>
          <div class='titulo'>
            ${this.user.nombreCompleto}
          </div>
          <div class='sub-titulo'>
            <p>${this.user.userName} - ${this.user.registro}</p>
          </div>
          <div class='sub-titulo'>
            <p>${this.user.perfil.nombre} - ${this.user.oficina.nombre} </p>
          </div>
          <div>
            <bbva-button-default id='btnRegresarHome'  @click='${ ()=>{this.regresarHome()}}' text='Regresar a bandeja'></bbva-button-default>
          </div>
        </div>
      </div>
    `;
  }
}

// Register the element with the browser
customElements.define(CellsUiProfileVcard.is, CellsUiProfileVcard);
